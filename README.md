[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

# NOTICE (March 2024)
Project has been archived, scheduled for deletion. Moved to https://gitlab.com/apl-ocean-engineering/raven/manipulation/bravo_ros/-/tree/f332c6b3101d009d8a77284da4319bf00a1978f5/bravo_gazebo.

# Bravo Gazebo

## About
Gazebo configuration for Bravo arm.

This repo contains world files, launch files, and configuration files for task specific simulation and the Gazebo simulated Bravo Arm controllers.

The simulator replaces the Bravo Arm setup and the ZED/Stereo Camera setup.

The rest of the pipeline is designed to be run as-is.

Here's a screenshot of the simulator running with the `object_detection` pipeline working on simulated images:

![screenshot](assets/rviz_screenshot.png)

## Usage

To launch, run:
`roslaunch bravo_gazebo simulation.launch`

## Updating Simulation Models
To add new models/CAD to the simulator, you must define a new model inside the `models/` folder. See the `kettlebell` model as an example.

You can then spawn the model inside the world (see the `/worlds/kettlebell_world.world` file as an example).
